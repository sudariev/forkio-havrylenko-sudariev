**_ Name _**

'Forkio Step Project by Yaroslav Havrylenko and Dmytrii Sudariev' for Dan-It.

**_ Description _**

Step Projact as a logical ending of Advanced HTML/CSS Module.

**_ List of technologies used _**

-   HTML, CSS, JS;
-   Git;
-   SASS/SCSS;
-   Gulp;
-   Node Plugins used: browser-sync, gulp, gulp-autoprefixer, gulp-clean,
-   gulp-clean-css, gulp-concat, gulp-imagemin, gulp-js-minify, gulp-rename,
-   gulp-sass, gulp-uglify, sass.

**_ Authors _**

    Yaroslav Havrylenko and Dmytrii Sudariev

**_ Tasks _**

**_ Made together as a team _**

-   Create project structure;
-   Set up assembly with Gulp;
-   Merge to branch Develop from branch Sudariev

**_ Yaroslav Havrylenko (Student #1 role) _**

-   Worked on branch Havrylenko
-   Implemented header including menu;
-   Implemented section 'People Are Talking About Fork';
-   Merged and resolved conflicts manually;
-   Published on Github pages.

**_ Dmytrii Sudariev (Student #2 role)_**

-   Worked on branch Sudariev
-   Implemented section 'Revolutionary Editor';
-   Implemented section 'Here is what you get';
-   Implemented section 'Fork Subscription Pricing';
-   Merged and resolved conflicts manually;
-   Published on Github pages.
